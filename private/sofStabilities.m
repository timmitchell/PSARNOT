function [c,c_grad] = sofStabilities(Amats,Bmats,Cmats,x,epsilon,radius,delta)
%   sofStabilities:
%       Computes the stability measures, along with their associated
%       gradients, of k SOF plants A_j + B_jXC_j that all share a single
%       controller matrix defined by the real-valued column vector x, where
%
%           X = reshape(x,p,m) is the shared controller matrix
%           p is the number of columns every B_j matrix has
%           m is the number of rows every C_j matrix has.
%
%       The A_j matrices must be square but are allowed to have differing
%       dimensions n_j, provided their corresponding B_j and C_j  matrices
%       all respectively have n_j number of rows and columns.
%
%       The (pseudo)spectral abscissa|radius is used to define the
%       stability measure.  The SOF plants can be continuous-time or
%       discrete-time.
%
%   NOTE:
%       PSARNOT requires the user to have the pseudospectral abscissa and
%       radius criss-cross algorithms, respectively pspa and pspr,
%       implemented by Emre Mengi and available from:
%
%       http://home.ku.edu.tr/~emengi/software/robuststability.html
%
%       installed and on the user's Matlab path.  These codes are not
%       required if one is interested in having the stability measures of
%       SOF plants only be defined with respect to the spectral abscissa
%       and radius.
%
%   USAGE:
%       [c,c_grad] = ...
%               sofStabilities(Amats,Bmats,Cmats,x,epsilon,radius,delta)
%
%   INPUT:
%       Amats       length k cell array of n_j by n_j "A" matrices
%
%       Bmats       length k cell array of n_j x p "B" matrices
%
%       Cmats       length k cell array of m x n_j "C" matrices
%
%       x           a real-valued p*m length column vector defining the
%                   controller matrix X for each SOF plant A_j + B_jXC_j
%
%       epsilon     length k vector of nonnegative values specifying the
%                   perturbation levels of the (pseudo)spectrums defining
%                   the stability measure for each SOF plant.  When
%                   epsilon(j) = 0, the spectral abscissa or radius is
%                   computed, while for epsilon(j) > 0, the
%                   epsilon-pseudospectral abscissa or radius is computed
%                   for the jth SOF plant.
%
%       radius      length k vector of logicals specifying whether the
%                   (pseudo)spectral:
%
%                       abscissa    (false, for continuous-time systems)
%                       radius      (true, for discrete-time systems)
%
%                   is employed in the stability measure for each SOF
%                   plant.
%
%       delta       length k vector of nonnegative values specifying
%                   separate stability safety margins for each SOF plant
%                   defined in Amats, Bmats, and Cmats.  A SOF plant is
%                   considered stable if its stability measure is at least
%                   distance delta inside the stable region.
%
%   OUTPUT:
%       c           a real-valued column vector of length k where each
%                   entry c(j) specifies whether the jth SOF plant is
%                   considered stable based off its (pseudo)spectral
%                   abscissa|radius and safety margin value deltas(j).
%                       c(j) <  0   ->  stable
%                       c(j) >= 0   ->  unstable
%
%       c_grad      matrix of k column vectors giving the corresponding
%                   gradient c(:,j) at x of each stability measure c(j),
%                   assuming c(j) is differentiable at x.  As the functions
%                   are differentiable almost everywhere, there is zero
%                   probability of requesting their gradients at nonsmooth
%                   points.  Nonetheless, in those rare events, the code
%                   will still return a "gradient" for each corresponding
%                   c(j) that can be used numerically, despite that it
%                   won't be valid.
%
%
%   If you publish work that uses or refers to PSARNOT (rhymes with Carnot,
%   as in car-no), either the included test problems or its code, please
%   cite the following paper:
%
%   [1] Frank E. Curtis, Tim Mitchell, and Michael L. Overton
%       A BFGS-SQP method for nonsmooth, nonconvex, constrained
%       optimization and its evaluation using relative minimization
%       profiles, Optimization Methods and Software, 32(1):148-181, 2017.
%       Available at https://dx.doi.org/10.1080/10556788.2016.1208749
%
%   For comments/bug reports, please visit the PSARNOT GitLab webpage:
%   https://gitlab.com/timmitchell/PSARNOT
%
%   PSARNOT Version 1.0, 2016, see AGPL license info below.
%   sofStabilities.m introduced in PSARNOT Version 1.0.
%
% =========================================================================
% |  PSARNOT:                                                             |
% |  (Pseudo)Spectral Abscissa|Radius Nonsmooth Optimization Test         |
% |  Copyright (C) 2016 Tim Mitchell                                      |
% |                                                                       |
% |  This file is part of PSARNOT.                                        |
% |                                                                       |
% |  PSARNOT is free software: you can redistribute it and/or modify      |
% |  it under the terms of the GNU Affero General Public License as       |
% |  published by the Free Software Foundation, either version 3 of       |
% |  the License, or (at your option) any later version.                  |
% |                                                                       |
% |  PSARNOT is distributed in the hope that it will be useful,           |
% |  but WITHOUT ANY WARRANTY; without even the implied warranty of       |
% |  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        |
% |  GNU Affero General Public License for more details.                  |
% |                                                                       |
% |  You should have received a copy of the GNU Affero General Public     |
% |  License along with this program.  If not, see                        |
% |  <http://www.gnu.org/licenses/>.                                      |
% =========================================================================

    % preallocate storage for the constraint values and gradients
    n_sof   = length(Amats);
    n_var   = length(x);
    c       = zeros(n_sof,1);
    c_grad  = zeros(n_var,n_sof);

    % evaluate all SOF plants
    for j = 1:n_sof
        [c(j),c_grad(:,j)] = sofStabilityMeasure(                       ...
                                Amats{j},   Bmats{j},   Cmats{j},       ...
                                x,          epsilon(j), radius(j),      ...
                                delta(j)                                );
    end
end
