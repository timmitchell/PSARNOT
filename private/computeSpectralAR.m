function [f,z,zR,zL] = computeSpectralAR(M,radius)
%   computeSpectralAR:
%       Computes the epsilon spectral abscissa or radius of matrix M.
%
%   USAGE:
%       [f,z,zR,zL] = computeSpectralAR(M,radius)
%
%   INPUT:
%       M       a square matrix, real or complex valued
%
%       radius  logical: abscissa (false), radius (true)
%
%   OUTPUT:
%       f       the spectral abscissa or radius of matrix M
%
%       z       the complex value specifying a globally rightmost
%               (abscissa) or outermost (radius) point of the spectrum for
%               matrix M
%
%       zR      the right eigenvector for z an eigenvalue of matrix M of
%               either largest real part (abscissa) or largestest modulus
%               (radius)
%
%       zL      the corresponding left eigenvector for z
%
%
%   If you publish work that uses or refers to PSARNOT (rhymes with Carnot,
%   as in car-no), either the included test problems or its code, please
%   cite the following paper:
%
%   [1] Frank E. Curtis, Tim Mitchell, and Michael L. Overton
%       A BFGS-SQP method for nonsmooth, nonconvex, constrained
%       optimization and its evaluation using relative minimization
%       profiles, Optimization Methods and Software, 32(1):148-181, 2017.
%       Available at https://dx.doi.org/10.1080/10556788.2016.1208749
%
%   For comments/bug reports, please visit the PSARNOT GitLab webpage:
%   https://gitlab.com/timmitchell/PSARNOT
%
%   PSARNOT Version 1.0, 2016, see AGPL license info below.
%   computeSpectralAR.m introduced in PSARNOT Version 1.0.
%
% =========================================================================
% |  PSARNOT:                                                             |
% |  (Pseudo)Spectral Abscissa|Radius Nonsmooth Optimization Test         |
% |  Copyright (C) 2016 Tim Mitchell                                      |
% |                                                                       |
% |  This file is part of PSARNOT.                                        |
% |                                                                       |
% |  PSARNOT is free software: you can redistribute it and/or modify      |
% |  it under the terms of the GNU Affero General Public License as       |
% |  published by the Free Software Foundation, either version 3 of       |
% |  the License, or (at your option) any later version.                  |
% |                                                                       |
% |  PSARNOT is distributed in the hope that it will be useful,           |
% |  but WITHOUT ANY WARRANTY; without even the implied warranty of       |
% |  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        |
% |  GNU Affero General Public License for more details.                  |
% |                                                                       |
% |  You should have received a copy of the GNU Affero General Public     |
% |  License along with this program.  If not, see                        |
% |  <http://www.gnu.org/licenses/>.                                      |
% =========================================================================

    % compute eigenvalues and right eigenvectors
    [X,lambda]      = eig(M);
    lambda          = diag(lambda);

    % compute eigenvalues (again) but this time, we obtain the left
    % eigenvectors of M conjugated.  In other words, the right eigenvectors
    % M transpose, when conjugated, are the left eigenvectors of M.
    [Y,lambdaleft]  = eig(M.');
    lambdaleft      = diag(lambdaleft);

    % get the eigenvalue of largest modulus (radius == true) or largest
    % real part (radius == false) and get its corresponding right and left
    % eigenvectors.

    % instead of check radius twice, set up functions
    if radius
        sort_fn     = @sortByModulus;
        get_f_fn    = @abs;
    else
        sort_fn     = @sortByRealPart;
        get_f_fn    = @real;
    end

    [sorted,index]  = sort_fn(lambda);
    z               = sorted(1);
    zR              = X(:,index(1));
    [eigdiff,j]     = min(abs(lambdaleft - z));
    zL              = conj(Y(:,j));

    if eigdiff > 1e-8*max(abs(lambda))
        warning(                                                        ...
            'computeSpectralAR:eigdiff',                                ...
            'eigenvalues of M and its transpose differ by %g', eigdiff  )
    end

    % convert z to the spectral abscissa or radius
    f = get_f_fn(z);
end
