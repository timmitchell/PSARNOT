function [m,m_grad] = sofStabilityMeasure(A,B,C,x,epsilon,radius,delta)
%   sofStabilityMeasure:
%       Computes the stability measure, along with its associated gradient,
%       of the SOF plant A + BXC for the controller matrix X where
%
%           A is a fixed n by n matrix
%           B is a fixed n by p matrix
%           C is a fixed m by n matrix
%           x is real-valued column vector of length p*m defining the
%           variable controller matrix X = reshape(x,p,m).
%
%       The (pseudo)spectral abscissa|radius is used to define the
%       stability measure.  The SOF plant can be a continuous-time or
%       discrete-time system.
%
%   NOTE:
%       PSARNOT requires the user to have the pseudospectral abscissa and
%       radius criss-cross algorithms, respectively pspa and pspr,
%       implemented by Emre Mengi and available from:
%
%       http://home.ku.edu.tr/~emengi/software/robuststability.html
%
%       installed and on the user's Matlab path.  These codes are not
%       required if one is interested in having the stability measures of
%       SOF plants only be defined with respect to the spectral abscissa
%       and radius.
%
%   USAGE:
%       [m,m_grad] = sofStabilityMeasure(A,B,C,x,epsilon,radius,delta)
%
%   INPUT:
%       A           n by n matrix
%
%       B           n by p matrix
%
%       C           n by m matrix
%
%       x           a real-valued p*m length column vector defining the
%                   controller matrix X for each SOF plant A_j + B_jXC_j
%
%       epsilon     a nonnegative value specifying the perturbation level
%                   of (pseudo)spectrum defining the stability measure.
%                   When epsilon = 0, the spectral abscissa or radius is
%                   computed, while for epsilon > 0, the epsilon
%                   pseudospectral abscissa or radius is computed.
%
%       radius      logical specifying whether the (pseudo)spectral:
%
%                       abscissa    (false, for continuous-time systems)
%                       radius      (true, for discrete-time systems)
%
%                   is employed in the stability measure for the SOF plant.
%
%       delta       nonnegative value specifying the stability safety m
%                   margin for the SOF plant.  A SOF plant is considered
%                   stable if its stability measure is at least distance
%                   delta inside the stable region.
%
%   OUTPUT:
%       c           real value specifying whether the SOF plant is
%                   considered stable based off its (pseudo)spectral
%                   abscissa|radius and safety margin value delta
%                       c <  0   ->  stable
%                       c >= 0   ->  unstable
%
%       c_grad      a real-valued p*m length column vector giving the
%                   corresponding gradient at x, assuming the stability
%                   measure is differentiable at x.  As the function is
%                   differentiable almost everywhere, there is zero
%                   probability of requesting its gradient at a nonsmooth
%                   point.  Nonetheless, in that rare event, the code will
%                   still return a "gradient" that can be used numerically,
%                   despite that it won't be valid.
%
%
%   If you publish work that uses or refers to PSARNOT (rhymes with Carnot,
%   as in car-no), either the included test problems or its code, please
%   cite the following paper:
%
%   [1] Frank E. Curtis, Tim Mitchell, and Michael L. Overton
%       A BFGS-SQP method for nonsmooth, nonconvex, constrained
%       optimization and its evaluation using relative minimization
%       profiles, Optimization Methods and Software, 32(1):148-181, 2017.
%       Available at https://dx.doi.org/10.1080/10556788.2016.1208749
%
%   For comments/bug reports, please visit the PSARNOT GitLab webpage:
%   https://gitlab.com/timmitchell/PSARNOT
%
%   PSARNOT Version 1.0, 2016, see AGPL license info below.
%   sofStabilityMeasure.m introduced in PSARNOT Version 1.0.
%
% =========================================================================
% |  PSARNOT:                                                             |
% |  (Pseudo)Spectral Abscissa|Radius Nonsmooth Optimization Test         |
% |  Copyright (C) 2016 Tim Mitchell                                      |
% |                                                                       |
% |  This file is part of PSARNOT.                                        |
% |                                                                       |
% |  PSARNOT is free software: you can redistribute it and/or modify      |
% |  it under the terms of the GNU Affero General Public License as       |
% |  published by the Free Software Foundation, either version 3 of       |
% |  the License, or (at your option) any later version.                  |
% |                                                                       |
% |  PSARNOT is distributed in the hope that it will be useful,           |
% |  but WITHOUT ANY WARRANTY; without even the implied warranty of       |
% |  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        |
% |  GNU Affero General Public License for more details.                  |
% |                                                                       |
% |  You should have received a copy of the GNU Affero General Public     |
% |  License along with this program.  If not, see                        |
% |  <http://www.gnu.org/licenses/>.                                      |
% =========================================================================

    % Compute the epsil-pseudospectral abscissa/radius of the SOF plant
    % M = A + BXC, where the controller matrix X is an appropriately
    % reshaped version of vector x
    M           = formABXC(A,B,C,x);
    [m,z,zR,zL] = computePSAR(M,epsilon,radius);

    % Matrix is M is stable (under perturbation) if its (pseudo)spectral
    % abscissa is negative or its (pseudo)spectral radius is less than one.
    % Thus, we shift the latter case by 1 so that m < 0 indicates stable
    % for both cases.
    if radius
        m = m - 1;
    end

    % delta >= 0 specifies the width of a margin of safety for the
    % stability boundary.  The pseudospectral abscissa/radius should be at
    % least distance delta inside the stable region in order for the
    % the measure to signify that M is stable.
    m = m + delta;

    % Compute the gradient of the stability measure (e.g. the gradient of
    % the pseudospectral abscissa/radius).
    m_grad = computeGradientPSAR(B,C,z,zR,zL,radius);
end
