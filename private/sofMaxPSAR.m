function [f,grad] = sofMaxPSAR(Amats,Bmats,Cmats,x,epsilon,radius,q)
%   sofMaxPSAR:
%       Computes the maximum of the (pseudo)spectral abscissa or radius
%       values, along with their associated gradients, over k SOF plants
%       A_j + B_jXC_j that all share a single controller matrix defined by
%       the real-valued column vector x, where
%
%           X = reshape(x,p,m) is the shared controller matrix
%           p is the number of columns every B_j matrix has
%           m is the number of rows every C_j matrix has.
%
%       The A_j matrices must be square but are allowed to have differing
%       dimensions n_j, provided their corresponding B_j and C_j  matrices
%       all respectively have n_j number of rows and columns.
%
%   NOTE:
%       PSARNOT requires the user to have the pseudospectral abscissa and
%       radius criss-cross algorithms, respectively pspa and pspr,
%       implemented by Emre Mengi and available from:
%
%       http://home.ku.edu.tr/~emengi/software/robuststability.html
%
%       installed and on the user's Matlab path.  These codes are not
%       required if one is only interested in computing the spectral
%       abscissa and radius of SOF plants.
%
%   USAGE:
%       [f,grad] = sofMaxPSAR(Amats,Bmats,Cmats,x,epsilon,radius,q)
%
%   INPUT:
%       Amats       length k cell array of n_j by n_j "A" matrices
%
%       Bmats       length k cell array of n_j x p "B" matrices
%
%       Cmats       length k cell array of m x n_j "C" matrices
%
%       x           a real-valued p*m length column vector defining the
%                   controller matrix X for each SOF plant A_j + B_jXC_j
%
%       epsilon     a nonnegative value specifying the perturbation level
%                   of (pseudo)spectrum defining the stability measure.
%                   When epsilon = 0, the spectral abscissa or radius is
%                   computed, while for epsilon > 0, the epsilon
%                   pseudospectral abscissa or radius is computed.
%
%       radius      logical specifying whether the (pseudo)spectral
%                   abscissa (false, for continuous-time systems) or radius
%                   (true, for discrete-time systems) is computed.
%
%       q           a nonnegative value specifying the amount, if any, of
%                   one-norm regularization added to the (pseudo)spectral
%                   abscissa|radius computed for each SOF plant defined by
%                   Amats, Bmats, and Cmats.  If  q = 0, no regularization
%                   is added.  Otherwise, when q > 0, each computed
%                   (pseudo)spectral abscissa or radius value will have
%                   q*norm(x,1) added to it, which will correspondingly
%                   change the maximal value return by this function, as
%                   well as its gradient.
%
%	OUTPUT:
%       f           the maximum (pseudo)spectral abscissa|radius computed
%                   for the k SOF plants defined in sof_data with the
%                   controller given by x.
%
%       grad        a real-valued p*m length column vector giving the
%                   corresponding gradient at x, assuming the function is
%                   differentiable at x.  Since the function is
%                   differentiable almost everywhere, there is zero
%                   probability of requesting the gradient at a nonsmooth
%                   point.  Nonetheless, in that rare event, the code will
%                   still return a "gradient" that can be used numerically,
%                   despite that it won't be valid.
%
%
%   If you publish work that uses or refers to PSARNOT (rhymes with Carnot,
%   as in car-no), either the included test problems or its code, please
%   cite the following paper:
%
%   [1] Frank E. Curtis, Tim Mitchell, and Michael L. Overton
%       A BFGS-SQP method for nonsmooth, nonconvex, constrained
%       optimization and its evaluation using relative minimization
%       profiles, Optimization Methods and Software, 32(1):148-181, 2017.
%       Available at https://dx.doi.org/10.1080/10556788.2016.1208749
%
%   For comments/bug reports, please visit the PSARNOT GitLab webpage:
%   https://gitlab.com/timmitchell/PSARNOT
%
%   PSARNOT Version 1.0, 2016, see AGPL license info below.
%   sofMaxPSAR.m introduced in PSARNOT Version 1.0.
%
% =========================================================================
% |  PSARNOT:                                                             |
% |  (Pseudo)Spectral Abscissa|Radius Nonsmooth Optimization Test         |
% |  Copyright (C) 2016 Tim Mitchell                                      |
% |                                                                       |
% |  This file is part of PSARNOT.                                        |
% |                                                                       |
% |  PSARNOT is free software: you can redistribute it and/or modify      |
% |  it under the terms of the GNU Affero General Public License as       |
% |  published by the Free Software Foundation, either version 3 of       |
% |  the License, or (at your option) any later version.                  |
% |                                                                       |
% |  PSARNOT is distributed in the hope that it will be useful,           |
% |  but WITHOUT ANY WARRANTY; without even the implied warranty of       |
% |  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        |
% |  GNU Affero General Public License for more details.                  |
% |                                                                       |
% |  You should have received a copy of the GNU Affero General Public     |
% |  License along with this program.  If not, see                        |
% |  <http://www.gnu.org/licenses/>.                                      |
% =========================================================================

    % COMPUTE the max of the pseudospectral abscissa/radius attained
    % by the SOF plants with controller x
    Mmats               = formABXCMatrices(Amats,Bmats,Cmats,x);
    [f,index,z,zR,zL]   = computeMaxPSAR(Mmats,epsilon,radius);

    % COMPUTE the corresponding gradient
    % The variable index specifies which SOF plant attained, or tied for,
    % the max pseudospectral abscissa/radius.  The gradient will be
    % computed based of that particular plant.
    B       = Bmats{index};
    C       = Cmats{index};
    grad    = computeGradientPSAR(B,C,z,zR,zL,radius);

    % Optionally add one norm regularization
    if q > 0
        f       = f + q*norm(x,1);
        grad    = grad + q*sign(x);
    end
end
