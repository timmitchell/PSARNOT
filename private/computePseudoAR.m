function [f,z,zR,zL] = computePseudoAR(M,epsil,radius)
%   computePseudoAR:
%       Computes the epsilon pseudospectral abscissa or radius of matrix M.
%
%   USAGE:
%       [f,z,zR,zL] = computePseudoAR(M,epsil,radius)
%
%   INPUT:
%       M       a square matrix, real or complex valued
%
%       epsil   a positive finite value specifying the perturbation level
%               of the pseudospectrum.
%
%       radius  logical: abscissa (false), radius (true)
%
%   OUTPUT:
%       f       the epsilon pseudospectral abscissa or radius of matrix M
%
%       z       the complex value specifying a globally rightmost
%               (abscissa) or outermost (radius) point of the
%               pseudospectrum for matrix M
%
%       zR      the right eigenvector for z an eigenvalue of matrix
%               M + Delta where Delta is the perturbation of norm epsilon
%               that gives the globally rightmost/outerpoint point z
%
%       zL      the corresponding left eigenvector for z
%
%
%   If you publish work that uses or refers to PSARNOT (rhymes with Carnot,
%   as in car-no), either the included test problems or its code, please
%   cite the following paper:
%
%   [1] Frank E. Curtis, Tim Mitchell, and Michael L. Overton
%       A BFGS-SQP method for nonsmooth, nonconvex, constrained
%       optimization and its evaluation using relative minimization
%       profiles, Optimization Methods and Software, 32(1):148-181, 2017.
%       Available at https://dx.doi.org/10.1080/10556788.2016.1208749
%
%   For comments/bug reports, please visit the PSARNOT GitLab webpage:
%   https://gitlab.com/timmitchell/PSARNOT
%
%   PSARNOT Version 1.0, 2016, see AGPL license info below.
%   computePseudoAR.m introduced in PSARNOT Version 1.0.
%
% =========================================================================
% |  PSARNOT:                                                             |
% |  (Pseudo)Spectral Abscissa|Radius Nonsmooth Optimization Test         |
% |  Copyright (C) 2016 Tim Mitchell                                      |
% |                                                                       |
% |  This file is part of PSARNOT.                                        |
% |                                                                       |
% |  PSARNOT is free software: you can redistribute it and/or modify      |
% |  it under the terms of the GNU Affero General Public License as       |
% |  published by the Free Software Foundation, either version 3 of       |
% |  the License, or (at your option) any later version.                  |
% |                                                                       |
% |  PSARNOT is distributed in the hope that it will be useful,           |
% |  but WITHOUT ANY WARRANTY; without even the implied warranty of       |
% |  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        |
% |  GNU Affero General Public License for more details.                  |
% |                                                                       |
% |  You should have received a copy of the GNU Affero General Public     |
% |  License along with this program.  If not, see                        |
% |  <http://www.gnu.org/licenses/>.                                      |
% =========================================================================

    % Compute the epsilon-pseudospectral radius/abscissa and also obtain a
    % corresponding eigenvalue that is a globally rightmost point of the
    % epsilon-pseudospectrum that lies on the imaginary axis (abscissa) or
    % unit circle (radius).
    %
    % This is done by calling the criss-cross algorithms implemented by
    % Emre Mengi et al, available here:
    % http://home.ku.edu.tr/~emengi/software/robuststability.html
    %
    if radius
        [f, z] = pspr(M, epsil, 0, 0, 0);
    else
        [f, z] = pspa(M, epsil, 0, 0, 0);
    end

    % if more than one eigenvalue is returned, just take the first one
    if length(z) > 1
        z = z(1);
    end

    % Obtain the right and left singular vectors of the associated
    % smallest singular value
    n       = length(M);
    [U,~,V] = svd(M - z*eye(n));
    zR      = V(:,n);
    zL      = U(:,n);
end
