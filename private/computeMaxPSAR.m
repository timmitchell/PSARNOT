function [max_f,index,z,zR,zL] = computeMaxPSAR(Mmats,epsil,radius)
%   computeMaxPSAR:
%       Computes the largest epsilon (pseudo)spectral abscissa or radius
%       attained by k matrices given in Mmats.
%
%   USAGE:
%       [max_f,index,z,zR,zL] = computeMaxPSAR(Mmats,epsil,radius)
%
%   INPUT:
%       Mmats   a cell array of k matrices to evaluate
%
%       epsil   a nonnegative value specifying the perturbation level for
%               computing the epsil-(pseudo)spectral abscissa or radius.
%               When epsil is set to 0, the spectral abscissa or radius is
%               computed.
%
%       radius  logical: abscissa (false), radius (true)
%
%   OUTPUT:
%       max_f   the max of the (pseudo)spectral abscissa or radius values
%               computed for the matrices in Mmats
%
%       index   specifies the index of which matrix in Mmats has the
%               largest (pseudo)spectral abscissa or radius.  If there are
%               ties, index will specifiy the first such matrix in Mmats.
%
%       z       the complex value specifying a globally rightmost
%               (abscissa) or outermost (radius) point of the
%               (pseudo)spectrum for matrix Mmats{index}
%
%       zR      the right eigenvector for z an eigenvalue of matrix
%               Mmats{index} + Delta where Delta is the perturbation of
%               norm epsilon that gives the globally rightmost/outerpoint
%               point z
%
%       zL      the corresponding left eigenvector for z
%
%
%   If you publish work that uses or refers to PSARNOT (rhymes with Carnot,
%   as in car-no), either the included test problems or its code, please
%   cite the following paper:
%
%   [1] Frank E. Curtis, Tim Mitchell, and Michael L. Overton
%       A BFGS-SQP method for nonsmooth, nonconvex, constrained
%       optimization and its evaluation using relative minimization
%       profiles, Optimization Methods and Software, 32(1):148-181, 2017.
%       Available at https://dx.doi.org/10.1080/10556788.2016.1208749
%
%   For comments/bug reports, please visit the PSARNOT GitLab webpage:
%   https://gitlab.com/timmitchell/PSARNOT
%
%   PSARNOT Version 1.0, 2016, see AGPL license info below.
%   computeMaxPSAR.m introduced in PSARNOT Version 1.0.
%
% =========================================================================
% |  PSARNOT:                                                             |
% |  (Pseudo)Spectral Abscissa|Radius Nonsmooth Optimization Test         |
% |  Copyright (C) 2016 Tim Mitchell                                      |
% |                                                                       |
% |  This file is part of PSARNOT.                                        |
% |                                                                       |
% |  PSARNOT is free software: you can redistribute it and/or modify      |
% |  it under the terms of the GNU Affero General Public License as       |
% |  published by the Free Software Foundation, either version 3 of       |
% |  the License, or (at your option) any later version.                  |
% |                                                                       |
% |  PSARNOT is distributed in the hope that it will be useful,           |
% |  but WITHOUT ANY WARRANTY; without even the implied warranty of       |
% |  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        |
% |  GNU Affero General Public License for more details.                  |
% |                                                                       |
% |  You should have received a copy of the GNU Affero General Public     |
% |  License along with this program.  If not, see                        |
% |  <http://www.gnu.org/licenses/>.                                      |
% =========================================================================

    max_f 	= -inf;
    index   = [];
    z       = [];
    zR      = [];
    zL      = [];

    for j = 1:length(Mmats)
        [fj,zj,zRj,zLj] = computePSAR(Mmats{j},epsil,radius);

        if fj > max_f
            max_f   = fj;
            index   = j;
            z       = zj;
            zR      = zRj;
            zL      = zLj;
        end
    end
end
