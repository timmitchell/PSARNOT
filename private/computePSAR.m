function [f,z,zR,zL] = computePSAR(M,epsil,radius)
%   computePSAR:
%       Computes the epsilon (pseudo)spectral abscissa or radius of
%       matrix M.
%
%   USAGE:
%       [f,z,zR,zL] = computePSAR(M,epsil,radius)
%
%   INPUT:
%       M       a square matrix, real or complex valued
%
%       epsil   nonnegative finite value specifying the perturbation level
%               of the pseudospectrum.  epsilon == 0 specifies the
%               spectrum.
%
%       radius  logical: abscissa (false), radius (true)
%
%   OUTPUT:
%       f       the (pseudo)spectral abscissa or radius of matrix M
%
%       z       the complex value specifying a globally rightmost
%               (abscissa) or outermost (radius) point of the
%               (pseudo)spectrum for matrix M
%
%       zR      the right eigenvector for z an eigenvalue of matrix
%               M + Delta where Delta is the perturbation of norm epsilon
%               that gives the globally rightmost/outerpoint point z
%
%       zL      the corresponding left eigenvector for z
%
%
%   If you publish work that uses or refers to PSARNOT (rhymes with Carnot,
%   as in car-no), either the included test problems or its code, please
%   cite the following paper:
%
%   [1] Frank E. Curtis, Tim Mitchell, and Michael L. Overton
%       A BFGS-SQP method for nonsmooth, nonconvex, constrained
%       optimization and its evaluation using relative minimization
%       profiles, Optimization Methods and Software, 32(1):148-181, 2017.
%       Available at https://dx.doi.org/10.1080/10556788.2016.1208749
%
%   For comments/bug reports, please visit the PSARNOT GitLab webpage:
%   https://gitlab.com/timmitchell/PSARNOT
%
%   PSARNOT Version 1.0, 2016, see AGPL license info below.
%   computePSAR.m introduced in PSARNOT Version 1.0.
%
% =========================================================================
% |  PSARNOT:                                                             |
% |  (Pseudo)Spectral Abscissa|Radius Nonsmooth Optimization Test         |
% |  Copyright (C) 2016 Tim Mitchell                                      |
% |                                                                       |
% |  This file is part of PSARNOT.                                        |
% |                                                                       |
% |  PSARNOT is free software: you can redistribute it and/or modify      |
% |  it under the terms of the GNU Affero General Public License as       |
% |  published by the Free Software Foundation, either version 3 of       |
% |  the License, or (at your option) any later version.                  |
% |                                                                       |
% |  PSARNOT is distributed in the hope that it will be useful,           |
% |  but WITHOUT ANY WARRANTY; without even the implied warranty of       |
% |  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        |
% |  GNU Affero General Public License for more details.                  |
% |                                                                       |
% |  You should have received a copy of the GNU Affero General Public     |
% |  License along with this program.  If not, see                        |
% |  <http://www.gnu.org/licenses/>.                                      |
% =========================================================================

    if epsil > 0
        [f,z,zR,zL] = computePseudoAR(M,epsil,radius);
    else
        [f,z,zR,zL] = computeSpectralAR(M,radius);
    end
end
