function Mmats = formABXCMatrices(Amats,Bmats,Cmats,xvec)
%   formABXCMatrices:
%       Returns a cell array of matrices of the form A{j} + B{j}XC{j} where
%       X is a single "controller" matrix formed by reshaping the column
%       vector xvec while the A, B, and C matrices are defined in the cell
%       arrays Amats, Bmats, and Cmats (which must all have the same number
%       of elements).
%
%   USAGE:
%       Mmats = formABXCMatrices(Amats,Bmats,Cmats,xvec)
%
%   INPUT:
%       Amats   a cell array of square n by n matrices, real or complex
%               valued
%
%       Bmats   a cell array of n by p matrices, real or complex valued
%
%       Cmats   a cell array of m by n matrix, real or complex valued
%
%       xvec    a column vector of length p*m
%
%   OUTPUT:
%       Mmats   a cell array of matrices of the form A + BXC for the given
%               controller X = reshape(xvec,p,m).
%
%
%   If you publish work that uses or refers to PSARNOT (rhymes with Carnot,
%   as in car-no), either the included test problems or its code, please
%   cite the following paper:
%
%   [1] Frank E. Curtis, Tim Mitchell, and Michael L. Overton
%       A BFGS-SQP method for nonsmooth, nonconvex, constrained
%       optimization and its evaluation using relative minimization
%       profiles, Optimization Methods and Software, 32(1):148-181, 2017.
%       Available at https://dx.doi.org/10.1080/10556788.2016.1208749
%
%   For comments/bug reports, please visit the PSARNOT GitLab webpage:
%   https://gitlab.com/timmitchell/PSARNOT
%
%   PSARNOT Version 1.0, 2016, see AGPL license info below.
%   formABXCMatrices.m introduced in PSARNOT Version 1.0.
%
% =========================================================================
% |  PSARNOT:                                                             |
% |  (Pseudo)Spectral Abscissa|Radius Nonsmooth Optimization Test         |
% |  Copyright (C) 2016 Tim Mitchell                                      |
% |                                                                       |
% |  This file is part of PSARNOT.                                        |
% |                                                                       |
% |  PSARNOT is free software: you can redistribute it and/or modify      |
% |  it under the terms of the GNU Affero General Public License as       |
% |  published by the Free Software Foundation, either version 3 of       |
% |  the License, or (at your option) any later version.                  |
% |                                                                       |
% |  PSARNOT is distributed in the hope that it will be useful,           |
% |  but WITHOUT ANY WARRANTY; without even the implied warranty of       |
% |  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        |
% |  GNU Affero General Public License for more details.                  |
% |                                                                       |
% |  You should have received a copy of the GNU Affero General Public     |
% |  License along with this program.  If not, see                        |
% |  <http://www.gnu.org/licenses/>.                                      |
% =========================================================================

    n               = length(Amats);
    Mmats           = cell(1,n);
    for j = 1:n
        Mmats{j}    = formABXC(Amats{j},Bmats{j},Cmats{j},xvec);
    end
end
