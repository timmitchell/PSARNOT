function g = computeGradientPSAR(B,C,z,x,y,radius)
%   computeGradientPSAR:
%       Computes the gradient of the epsilon (pseudo)spectral abscissa or
%       radius of the static output feedback (SOF) plant A + BXC with
%       respect to the controller variables X.  Note that X is not needed
%       since X determines input arguments z,x,y, which are needed.
%
%   USAGE:
%       g = computeGradientPSAR(B,C,z,x,y,radius)
%
%   INPUT:
%       B       matrix B from SOF plant A + BXC
%
%       C       matrix B from SOF plant A + BXC
%
%       z       complex value corresponding to a globally rightmost
%               (abscissa) or outermost (radius) point of the
%               (pseudo)spectrum
%
%       x       the right eigenvector for z an eigenvalue of
%                   A + BXC + Delta
%               where Delta is the perturbation of norm epsilon that gives
%               the globally rightmost/outerpoint point z
%
%       y       the corresponding left eigenvector for z
%
%       radius  logical: abscissa (false), radius (true)
%
%   OUTPUT:
%       g       the gradient of the epsilon (pseudo)spectral abscissa or
%               radius of the SOF plant A + BXC with respect to the
%               controller variables X, assuming the function is
%               differentiable at X. Since the epsilon (pseudo)spectral
%               abscissa or radius is differentiable almost everywhere,
%               there is zero probability of requesting the gradient at a
%               nonsmooth point.  Nonetheless, in that rare event, the code
%               will still return a "gradient" that can be used
%               numerically, despite that it won't be valid.
%
%
%   If you publish work that uses or refers to PSARNOT (rhymes with Carnot,
%   as in car-no), either the included test problems or its code, please
%   cite the following paper:
%
%   [1] Frank E. Curtis, Tim Mitchell, and Michael L. Overton
%       A BFGS-SQP method for nonsmooth, nonconvex, constrained
%       optimization and its evaluation using relative minimization
%       profiles, Optimization Methods and Software, 32(1):148-181, 2017.
%       Available at https://dx.doi.org/10.1080/10556788.2016.1208749
%
%   For comments/bug reports, please visit the PSARNOT GitLab webpage:
%   https://gitlab.com/timmitchell/PSARNOT
%
%   PSARNOT Version 1.0, 2016, see AGPL license info below.
%   computeGradientPSAR.m introduced in PSARNOT Version 1.0.
%
% =========================================================================
% |  PSARNOT:                                                             |
% |  (Pseudo)Spectral Abscissa|Radius Nonsmooth Optimization Test         |
% |  Copyright (C) 2016 Tim Mitchell                                      |
% |                                                                       |
% |  This file is part of PSARNOT.                                        |
% |                                                                       |
% |  PSARNOT is free software: you can redistribute it and/or modify      |
% |  it under the terms of the GNU Affero General Public License as       |
% |  published by the Free Software Foundation, either version 3 of       |
% |  the License, or (at your option) any later version.                  |
% |                                                                       |
% |  PSARNOT is distributed in the hope that it will be useful,           |
% |  but WITHOUT ANY WARRANTY; without even the implied warranty of       |
% |  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        |
% |  GNU Affero General Public License for more details.                  |
% |                                                                       |
% |  You should have received a copy of the GNU Affero General Public     |
% |  License along with this program.  If not, see                        |
% |  <http://www.gnu.org/licenses/>.                                      |
% =========================================================================

    yhB     = y'*B;
    Cx      = C*x;
    yhx     = y'*x;

    % form the gradient of a'Xb with respect matrix X (the controller)
    % and fixed vectors a = B'y and b = Cx
    g       = (yhB.'*Cx.') / yhx;
    g       = g(:);

    if radius
        mu  = conj(z)/abs(z);
        g   = mu*g;
    end
    g = real(g);

end
