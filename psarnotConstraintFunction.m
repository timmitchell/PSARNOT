function [c,c_grad] = psarnotConstraintFunction(sof_data,x)
%   psarnotConstraintFunction:
%       Computes the stability measures, along with their associated
%       gradients, of k SOF plants A_j + B_jXC_j that all share a single
%       controller matrix defined by the real-valued column vector x, where
%
%           X = reshape(x,p,m) is the shared controller matrix
%           p is the number of columns every B_j matrix has
%           m is the number of rows every C_j matrix has.
%
%       The A_j matrices must be square but are allowed to have differing
%       dimensions n_j, provided their corresponding B_j and C_j  matrices
%       all respectively have n_j number of rows and columns.
%
%       The (pseudo)spectral abscissa|radius is used to define the
%       stability measure.  The SOF plants can be continuous-time or
%       discrete-time.
%
%   NOTE:
%       The PSARNOT package requires the user to have the pseudospectral
%       abscissa and radius criss-cross algorithms, respectively pspa and
%       pspr, implemented by Emre Mengi and available from:
%
%       http://home.ku.edu.tr/~emengi/software/robuststability.html
%
%       installed and on the user's Matlab path.  However, if one is only
%       interested in using psarnotConstraintFunction to compute stability
%       measures of SOF plants based on the spectral abscissa or radius,
%       then these these two codes are not required.
%
%   USAGE:
%       [c,c_grad] = psarnotConstraintFunction(sof_data,x)
%
%   INPUT:
%       sof_data    a struct containing subfield .ineq which defines the
%                   fixed matrices for k SOF plants.  The subfields of
%                   .ineq are:
%
%       .ineq.Amats     length k cell array of n_j by n_j "A" matrices
%
%       .ineq.Bmats     length k cell array of n_j x p "B" matrices
%
%       .ineq.Cmats     length k cell array of m x n_j "C" matrices
%
%       .ineq.epsilon   specifies whether spectral (epsilon == 0) or
%                       pseudospectral (epsilon > 0) is employed as the
%                       stability measure.
%
%       .ineq.radius    logical specifying whether the (pseudo)spectral:
%
%                           abscissa (false, for continuous-time systems)
%                           radius (true, for discrete-time systems)
%
%                       is employed in the stability measure.
%
%       .ineq.delta     length k vector of nonnegative values specifying
%                       separate stability safety margins for each SOF
%                       plant defined by Amats, Bmats, and Cmats.  A SOF
%                       plant is considered stable if its stability measure
%                       is at least distance delta inside the stable
%                       region.
%
%       x           a real-valued p*m length column vector defining the
%                   controller matrix X for each SOF plant A_j + B_jXC_j
%
%   OUTPUT:
%       c           a real-valued column vector of length k where each
%                   entry c(j) specifies whether the jth SOF plant is
%                   considered stable based on its (pseudo)spectral
%                   abscissa|radius and safety margin value deltas(j).
%                       c(j) <  0   ->  stable
%                       c(j) >= 0   ->  unstable
%
%       c_grad      matrix of k column vectors giving the corresponding
%                   gradient c(:,j) at x of each stability measure c(j),
%                   assuming c(j) is differentiable at x.  As the
%                   constraints are differentiable almost everywhere, there
%                   is zero probability of requesting their gradients at
%                   nonsmooth points.  Nonetheless, in those rare events,
%                   the code will still return a "gradient" for each
%                   corresponding c(j) that can be used numerically,
%                   despite that it won't be valid.
%
%
%   This function, with .ineq.delta = zeros(1,k) and .ineq.radius = true,
%   can compute the value and gradient of the objective functions for the
%   nonconvex, nonsmooth, and constrained optimization problems defined by
%   equations (14) and (15) in [1], which respectively are:
%       non-locally-Lipschitz spectral radius   (.ineq.epsilon = 0)
%       locally Lipschitz pseudospectral radius (.ineq.epsilon > 0)
%   SOF controller design problems, with the nonstandard convention that
%   c(j) <= 0 is considered stable and c(j) > 0 is considered unstable.
%
%   For computing the corresponding objective functions of (14) and (15),
%   see also psarnotObjectiveFunction.
%
%   Data files sof_data_sradius.mat and sof_data_psradius.mat each contain
%   a cell array "sof_data" that respectively contains 100 spectral radius
%   problems of the form given by (14) and  100 pseudoradius problems of
%   the form given by (15).  These 200 problems were used in the benchmarks
%   contained in [1].  Each struct element must contain subfields:
%       .obj    (as described in psarnotObjectiveFunction)
%       .ineq   (as described here, in psarnotConstraintFunction)
%   The additional subfields n_var, n_ineq, and stats contain attributes
%   for each specific problem instance but they are only for the user's
%   benefit; all functions in the PSARNOT package will ignore any other
%   fields besides .obj and .ineq.
%
%   New problem instances may be easily generated as long as they adhere to
%   the same structure format, though for the psarnotConstraintFunction, it
%   will generally be desirable to specify stable matrices for all the
%   matrices in .ineq.Amats so that it will be a priori known that all
%   constraints are satisfiable (as X = zeros(p,m) will be a feasible
%   solution).  While the dimension of the square matrices in Amats may
%   vary over the SOF plants, the dimensions p and m cannot vary over the
%   SOF plants defined in .obj and .ineq.  Furthermore, one benefit of
%   using discrete-time SOF plants, and thus the (pseudo)spectral radius,
%   is that then it is known the objective function will bounded below
%   everywhere. On the other hand, for continuous-time SOF plants, the
%   (pseudo)spectral abscissa may be unbounded below on and/or off the
%   feasible set, which can present an additional challenge for
%   optimization methods.
%
%
%   If you publish work that uses or refers to PSARNOT (rhymes with Carnot,
%   as in car-no), either the included test problems or its code, please
%   cite the following paper:
%
%   [1] Frank E. Curtis, Tim Mitchell, and Michael L. Overton
%       A BFGS-SQP method for nonsmooth, nonconvex, constrained
%       optimization and its evaluation using relative minimization
%       profiles, Optimization Methods and Software, 32(1):148-181, 2017.
%       Available at https://dx.doi.org/10.1080/10556788.2016.1208749
%
%   For comments/bug reports, please visit the PSARNOT GitLab webpage:
%   https://gitlab.com/timmitchell/PSARNOT
%
%   PSARNOT Version 1.0, 2016, see AGPL license info below.
%   psarnotConstraintFunction.m introduced in PSARNOT Version 1.0.
%
% =========================================================================
% |  PSARNOT:                                                             |
% |  (Pseudo)Spectral Abscissa|Radius Nonsmooth Optimization Test         |
% |  Copyright (C) 2016 Tim Mitchell                                      |
% |                                                                       |
% |  This file is part of PSARNOT.                                        |
% |                                                                       |
% |  PSARNOT is free software: you can redistribute it and/or modify      |
% |  it under the terms of the GNU Affero General Public License as       |
% |  published by the Free Software Foundation, either version 3 of       |
% |  the License, or (at your option) any later version.                  |
% |                                                                       |
% |  PSARNOT is distributed in the hope that it will be useful,           |
% |  but WITHOUT ANY WARRANTY; without even the implied warranty of       |
% |  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        |
% |  GNU Affero General Public License for more details.                  |
% |                                                                       |
% |  You should have received a copy of the GNU Affero General Public     |
% |  License along with this program.  If not, see                        |
% |  <http://www.gnu.org/licenses/>.                                      |
% =========================================================================

    d           = sof_data.ineq;
    k           = length(d.Amats);
    epsilon     = d.epsilon * ones(1,k);
    radius      = logical(d.radius) .* true(1,k);
    [c,c_grad]  = sofStabilities(   d.Amats,    d.Bmats,    d.Cmats,    ...
                                    x,                                  ...
                                    epsilon,    radius,     d.delta     );
end
