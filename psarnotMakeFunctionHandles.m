function [n,obj_fn,ineq_fn,all_fn] = psarnotMakeFunctionHandles(sof_data)
%   psarnotMakeFunctionHandles:
%       Convenience function which takes a single problem instance from
%       either sof_data_sradius.mat or sof_data_psradius.mat (or another
%       similarly formatted problem instance) and makes the corresponding
%       function handles so that various optimization codes can run on this
%       problem, such as GRANSO: GRadient-based Algorithm for NonSmooth
%       Optimization or fmincon.
%
%   USAGE:
%       [n,obj_fn,ineq_fn,all_fn] = psarnotMakeFunctionHandles(sof_data)
% 
%   INPUT:
%       sof_data    a struct containing subfield .ineq which defines the 
%                   fixed matrices for k SOF plants.  The subfields of
%                   .ineq are:
%   
%       .ineq.Amats     length k cell array of n_j by n_j "A" matrices 
%
%       .ineq.Bmats     length k cell array of n_j x p "B" matrices
%
%       .ineq.Cmats     length k cell array of m x n_j "C" matrices
%
%       .ineq.epsilon   specifies whether spectral (epsilon == 0) or 
%                       pseudospectral (epsilon > 0) is employed as the 
%                       stability measure.
%
%       .ineq.radius    logical specifying whether the (pseudo)spectral:
%
%                           abscissa (false, for continuous-time systems) 
%                           radius (true, for discrete-time systems)
%
%                       is employed in the stability measure.
% 
%       .ineq.delta     length k vector of nonnegative values specifying 
%                       separate stability safety margins for each SOF 
%                       plant defined by Amats, Bmats, and Cmats.  A SOF 
%                       plant is considered stable if its stability measure
%                       is at least distance delta inside the stable
%                       region.  
% 
%   OUTPUT:
%       n           the number of optimization variables (n = p*m).  All 
%                   function handles will take a single column vector x of 
%                   length n as input.
%
%       obj_fn      a function handle of single column vector x which
%                   evaluates the objective and its gradient at x.  For
%                   further details, type 'help psarnotObjectiveFunction'.
%
%       ineq_fn     a function handle of single column vector x which
%                   evaluates the inequality constraints and their
%                   gradients at x. For further details, type 'help
%                   psarnotConstraintFunction'.
%
%       all_fn      a function handle of column vector x which 
%                   simultaneously evaluates the objective and constraint 
%                   functions:
%
%                   [f,f_grad,ci,ci_grad,ce,ce_grad] = all_fn(x)
%               
%                   [f,f_grad]:     psarnotObjectiveFunction's output args
%                   [ci,ci_grad]:   psarnotConstraintFunction's output args
%                   [ce,ce_grad]:   Both variables are always empty arrays.
%     
%   See also psarnotObjectiveFunction and psarnotConstraintFunction.
%
%
%   If you publish work that uses or refers to PSARNOT (rhymes with Carnot, 
%   as in car-no), either the included test problems or its code, please 
%   cite the following paper:
%
%   [1] Frank E. Curtis, Tim Mitchell, and Michael L. Overton 
%       A BFGS-SQP method for nonsmooth, nonconvex, constrained 
%       optimization and its evaluation using relative minimization 
%       profiles, Optimization Methods and Software, 32(1):148-181, 2017.
%       Available at https://dx.doi.org/10.1080/10556788.2016.1208749
%
%   For comments/bug reports, please visit the PSARNOT GitLab webpage:
%   https://gitlab.com/timmitchell/PSARNOT
%
%   PSARNOT Version 1.0, 2016, see AGPL license info below.
%   psarnotMakeFunctionHandles.m introduced in PSARNOT Version 1.0.
%
% =========================================================================
% |  PSARNOT:                                                             |
% |  (Pseudo)Spectral Abscissa|Radius Nonsmooth Optimization Test         |
% |  Copyright (C) 2016 Tim Mitchell                                      |
% |                                                                       |
% |  This file is part of PSARNOT.                                        |
% |                                                                       |
% |  PSARNOT is free software: you can redistribute it and/or modify      |
% |  it under the terms of the GNU Affero General Public License as       |
% |  published by the Free Software Foundation, either version 3 of       |
% |  the License, or (at your option) any later version.                  |
% |                                                                       |
% |  PSARNOT is distributed in the hope that it will be useful,           |
% |  but WITHOUT ANY WARRANTY; without even the implied warranty of       |
% |  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        |
% |  GNU Affero General Public License for more details.                  |
% |                                                                       |
% |  You should have received a copy of the GNU Affero General Public     |
% |  License along with this program.  If not, see                        |
% |  <http://www.gnu.org/licenses/>.                                      |
% =========================================================================

    n       = sof_data.n_var;

    d       = sof_data.obj; 
    obj_fn  = @(x) sofMaxPSAR(      d.Amats,    d.Bmats,    d.Cmats,    ...
                                    x,                                  ...
                                    d.epsilon,  d.radius,   d.q         );


    d       = sof_data.ineq;
    k       = length(d.Amats);
    epsilon = d.epsilon * ones(1,k);
    radius  = logical(d.radius) .* true(1,k);
    ineq_fn = @(x) sofStabilities(  d.Amats,    d.Bmats,    d.Cmats,    ...
                                    x,                                  ...
                                    epsilon,    radius,     d.delta     );
                                
    all_fn  = @evaluateAll;
   
    function [f,f_grad,ci,ci_grad,ce,ce_grad] = evaluateAll(x)

        [f,f_grad]        = obj_fn(x);
        [ci,ci_grad]    = ineq_fn(x);
        ce              = [];
        ce_grad         = [];
    end
end